// react API addcomment.js
var DBconf = require('./DBconf');
var async = require('async');
var knex = require('knex')({
  client:'mysql',
  connection:{ host : DBconf.db.host, user : DBconf.db.usr, password : DBconf.db.pswd, database : DBconf.db.DB },
  pool: { min: 0, max: 3 }
});
module.exports = function(app) {
	app.post('/react/api/add', function(req, res) { console.log('!!!!!!!!!!!! /react/api/add->req:', req.body);
	  async.series([
				 function(cb){
				  knex.insert([
					{
					  rc_name:req.body.author,
					  rc_comm:req.body.comments
					}
				  ])
				  .into('r_comm')
				  .then(function(rez){ // console.log('insREZ:', rez[0]);
					cb();
				  });
				}],
			  function(err,results){
				res.status(200).json({error:err,results: results});
			  }
	  );
	});
}