var React = require('react');

var About = React.createClass({
	render: function() {
		return (<div>
			<h4><hr />About Page or My Way to React<hr /></h4>
			There's nothing special about ReactJS, indeed.<br />
			It didn't produce a wow effect, and it looks like it is under heavy development yet: I've browsed few books, spent few hours watching various courses and youtube
			 crappy 'lessons' and found few different versions of some very common <em>design patterns</em> (see my list of <u>notes</u> below).
			Anyway, even for me, a newcomer to this library, there're quite a few things
			to learn, like and, no doubt, dislike.<br />
			Absence of its own, built-in AJAX-like methods, rather <em>special</em> form data handling, events, states... yet a real capper got me down:
			they use babeljs for all JS things as no browser (yet) is capable of handling the advanced JS features, that's why it is necessary to downgrade the JS syntax!
			 <br /><i>(just no comments)</i><br />
			All those made me missing NG, really. Even a mix of plain old <em>paragraph</em> tags along with <em>breaks</em> and <em>horizontals</em>&nbsp;
			forced me rewriting few things in this very text (it looks like ReactJS can't stand nested tags, can it?)...<br />
			OK, now few words on what this is all about.
			<ul>
				<li>I staged the whole SPA from express-react npm - a handy thing.</li>
				<li>...added MySQL DB (actually, one single table into an existing DB on my VPS).</li>
				<li>...ExpressJS is an outstanding library as here it serves both the server and the client part of the application - really, like it.</li>
				<li>The stage gave me a robust development environment, which helps a lot on start.</li>
			</ul>
			I'm not going to list all my 'pros' and 'cons' here, for myself I found only one viable excuse why someone may strive for ReactJS usage (except
			, maybe, for fb guys themselves): server-side UI and data baking, which is also might not be the best choice in many situations imho. The irony
			for myself lies in the fact I watched somewhat long <u>MVC</u> epopee, which was popularizing the idea of source code and UI separation, and
			in React we have a horrible mix of these... just wonder, what's the next on the way of such technologies that come and go in fashion?<br />
			Right now I have a few <em>hands-on</em> questions to ask about 'how-to...', but I think that I won't get the answers in no time, as usual 
			:) - <stress>Things Take Time</stress> I know.
			<br />
			So, the actual project meets, at minimum, the requirements of the assignment and I, as a newbie, feel myself pretty satisfied after this part of my weekend!<br />
			<span className='smallprint'>alex</span>
		</div>);
	}
});

module.exports = About;
