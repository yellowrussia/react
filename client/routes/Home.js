var React = require('react');
  const modalStyle = {
	position: 'fixed',
	zIndex: 1550,
	top: 0, bottom: 0, left: 0, right: 0
  };

  const backdropStyle = {
	...modalStyle,
	zIndex: 'auto',
	backgroundColor: '#000',
	opacity: 'inherit'
  };
  const displayNone = function(){
  	return{
  		display:'none'
  	};
  };
  const dialogStyle = function() {
	// we use some psuedo random coords so nested modals
	// don't sit right on top of each other.
	let top = 50;
	let left = 50;

	return {
	  position: 'absolute',
	  width: 500,
	  top:'35%', left:'45%',
	  transform: `translate(-${top}%, -${left}%)`,
	  border: '1px solid #e5e5e5',
	  backgroundColor: 'white',
	  boxShadow: '0 5px 15px rgba(0,0,0,.5)',
	  padding: 20
	};
  };
var ModalCite = React.createClass({
	  getInitialState: function() {
	    return {author: '', comments: this.props.textToCite, showModalDiv: true};
	  },
	  handleSubmit: function(e) {
	    e.preventDefault();
	    var author = this.state.author.trim();
	    var comments = this.state.comments.trim();
	    if (!comments || !author) {
	      return;
	    }
	    $.ajax({
	      url: this.props.url,
	      dataType: 'json',
	      type: 'POST',
	      data: {author:author,comments:comments},
	      success: function(data) {
	        this.setState({data: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	        this.setState({data: comments});
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
		//rc_name:req.body.author,
		//rc_comm:req.body.comments
	    console.log('author:', author);
	    console.log('comments:', comments);
	    this.setState({author: '', comments: '', showModalDiv: false});
	    window.location.href='/';
	  },
	  handleAuthorChange: function(e) {
	    this.setState({author: e.target.value});
	  },
	  handleCommentsChange: function(e) {
	    this.setState({comments: e.target.value});
	  },
  	render: function (){
  		var curStyle = (this.state.showModalDiv ? dialogStyle() : displayNone());
		return (
				<div style={curStyle}>
	            <h4 id='modal-label'>Wanna Cite? Shoot!</h4>
	            <small className="smallprint">NB: 'Cancel' button is on my TO-DO list...</small>
	            <p>
	            	<form onSubmit={ this.handleSubmit }>
	            		<p><input type='text' onChange={this.handleAuthorChange} value={this.state.author} placeholder="please, name yourself"/></p>
	            		<textarea className="styled" onChange={this.handleCommentsChange} value={this.state.comments} />
	            		<p><button className='btn'>Cite it</button>
	            		{/* TO-DO: <button onCancel={()=>{ this.cleanForm() }}>Reset/Cancel</button>*/}
	            		</p>
	            	</form>
	            </p>
	          </div>
		);
  	}
});
var Onecomm = React.createClass({
  render: function() {
    let data = this.props.data;
    return (
      <li>
        <div><u>{data.rc_name}</u>:
        	<p className='combox'>{data.rc_comm}</p>
        </div>
      </li>
    );
  }
});
var Comms = React.createClass({
  getInitialState: function() {
	    return {
	      comms: []
	    }
	},  
    componentDidMount: function(){
	    $.ajax({
	      url: this.props.url,
	      dataType: 'json',
	      type: 'GET',
	      success: function(data) {
	        this.setState({comms:data.results});
	        console.log('Comms-data-results:', data.results);
	      }.bind(this),
	      error: function(xhr, status, err) {
	        this.setState({data: comments});
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
  	},
	render: function(){
		let listComms = this.state.comms.map((comObj)=>{
			return <Onecomm key={comObj.rc_id} data={comObj} />;
		});
		return(<div>
				<h3><hr />Comments<hr /></h3>
				<ul>
					{listComms}
				</ul>
			   </div>);
	}
});
var Home = React.createClass({
	getInitialState(){
	  return { textToCite: null };
	},
	textSelection: function(e){
		// e.preventDefault();
	  if (window.getSelection) {
		if(window.getSelection().toString().length){
		  this.setState({ textToCite: window.getSelection().toString()});
		}
	  }
	},
	componentDidMount:function(){
	  window.addEventListener('click', this.textSelection, false); // ???  onSubmit={this.onButtonClick}
	},
	render: function() {
		return (
			<div>
			  <h1>Bad to the Bone 25 Anniversary</h1><h4>George Thorogood &amp; The Destroyers</h4>
			  {this.state.textToCite ? <ModalCite style={modalStyle} url="/react/api/add" backdropStyle={backdropStyle} textToCite={this.state.textToCite} /> : null }
			   
			  <p><img className="postimg" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABQODxIPDRQSEBIXFRQYHjIhHhwcHj0sLiQySUBMS0dARkVQWnNiUFVtVkVGZIhlbXd7gYKBTmCNl4x9lnN+gXz/2wBDARUXFx4aHjshITt8U0ZTfHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHz/wgARCAEmASwDAREAAhEBAxEB/8QAGgAAAgMBAQAAAAAAAAAAAAAAAAEDBAUCBv/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/aAAwDAQACEAMQAAAB146AAAAAKIAAAAIKAAAABQAAAOgAAK83ipOySpDs7Ojquzo6GdDGAAAhAciEcnJycnERnBwRnEcLoWZ0vJfqQoRwIANOs6LVWSvFQ5Ji9XIzMjkYDLJZqnEIxAAQUAXUpqjUrtKctymVyc7MeEa9VSciKEaVVI0K4K5OIzIuliq8TV0QE4yrFioYol1Kao1KR0IR0RE6ZUvJsWQkJZXKjRqpGlVMsgcmdFslro6K8T1GdHQEZnRdSmqLJ1VeJxkJNSiqIt1BFmoIiJyIsEQyU5KpYJagjslqCOjojLBXIS8UQjR1NCsmK0qEMR0cgI7OTf1OSMx83k7OBgdHAxDGI6ORmzqYubeKIHpNTPixVojEMAOjoiJqxsrBGa9RkBIckpyIrlo6OAICYgLRLXlsW+meoeh1AiLRGIDusfLYpnNOMqJiM1KrkxGdlKLNM5Jjs5ERjESkp5fNvWUJQ6JiAkEckhGcmnqXDDzZCEYDEIlIgAlIzb1IIzJWcExASARmjZnShKTWB0V5ZrNqvOZvo9SArxmy2LIZZTc1ITEiwVpbVmnWXCIomqMhiapCvE1I7KMuhZQlR6XUiJyuTkRNXm8X0u5XhE4VHGTlf0lJzkjGSVBCLJTLQxHQjkhOjAzb1lGVG1qTAUos1IdHns3d1KxzFipSmVctjSscFghLJwVIjLVWTNjWrFjVqA4jgvV57Nv2UJUEMApiFAFMBAMAAQhgAAAy5YyjKAADEaCUFQAMAAQAMBAADGcgAAMBDAQAADEBespSoBgMQCAAGAgAZ0cjNSqpWgOQAAEAwEAF9KKkFAAACAAGAxABKTVeLpUMuIjkQxAAAAABoWUJUMAAAhUAAAAxRLWjVomORFNKctUAEAAADEBo2UJUIYDEACCCmIAA2qkK4hF0iKhTiMAAQwAAEaNlGXkYAACEMQDEBZIjerspEYF8xoREcDEAAAAAGjZnygAAgAQwEMANquCUtjOQAzCwZUQgAAMBAAGjZnyggAAABDEAwLxaoLZIIjOzPLhmFCAAAYAAAaNmdKxAMBAIYhgAASGnVkkOCArFWNKsuIhAAAMAADUsy5QAAAEAAMAGI7NCrR0cCIyhFiqEdgI5EIYAAjVsy5QlOSyBXGdllJLWSnYyMokZoEpRLQGZE9aRMdCEcGHEACGBrWZMsh6KkSoCGAAAAAFIy5e6unZ0RGfE1m0oAABWPPwAAjWsypdOpy0SIwGAhgIYCXHAqxfqYrFUijg2qvIKAMRgxUAANXUys2ciOiQ7Ja1Ts5AAGBAZZMBVi3VozormjZdXolABoGcuNAAGtZkyoAGAy0adTCA7OhGZDroiISGODss1rnQCA6AZWPPQABrWZMoIYAAGjWodJ0oIDz8dGrXRwBEUC+XxiAQHQHB5qEAGtZkygAIYAb1WwQUEQnnYCYu10RRSOD0dSgAxAAFMxIBAa1mTKAIBgWjfoAAEZhlQAIAAum5QAhjAAIjDiuAzVsyJQBgAG7ZcGJRGcrgRWAAEAG5V4RGdHYDBEoVDBhga1mRKAAwJj0VjAStEsZ5yEIQAM6PRWdHagDBAFQAeeiARrWZMqAYDNZNKmJQAM4x4AEAwLxbs6qaGrAZwnYlAM+MwDXsx5QmAjA9FZMoAABhRUAAEAzaoRUgEMmjteU7OTleDDgNrUyc1J3TOZejcsAEAxGDLyIYJ2vIJ6C2JOlaRnQHZ2MSgiExo7ras/8QAKxAAAgIBAwIHAQACAwEAAAAAAQIAAxEEEhMQFCAhIjAxMjNBI0A0QlBD/9oACAEBAAEFAs/+M9jbuaydxaJ3Vk7x53k70TvFnd1zuqp3NU7iqc9c5a5yJORJvSb0m9ZyJORJy1zlrnPVO4qncVTuqp3Vc7yud4s72d4Z3dk7q2dxbOV5uMosIQ/MrRSnEkddreBUUo32pUFdiy5VC4PSoAvxpDWm3jSY8NKhpxLLE2H2avq3zKvzC4jgtcAEAwYU3WkhejfYE9Kl8t65tTIlX0tJ3wMDGrzZ5ICosWUfGPXf5lUCAFXmzbacTyYJX6iwBtQYlX1PzKvzXfvJG6xdy1LtUH12oWijCt9ulRzWaiXc4WVfnb+kon/ZiQMtG820/wAHfvf6sMrUhUn73fSv81hJy7HbKvqfmLdtXnjMWIuOOcxnJbnM5zD8znMVys54zl+i27VY7m54j7I9m6C+NdnpXZsnPHsLxLSs54LPW9u4LbtXkIfnEewt0q+p+Zp9PvHbVS+niIBPUqRMTaT4NpHTtao2mqCvp6xVtOJtPTBmJtI6YPQDMII6bW61aetqpV9T8xvRRp6i0soZ5jjS1OSqlFqrrtS6VV8bPclbKq0pXYtgWscoLb9VWEfRHz1h/wAucJW4tSmkIVvRrBUEvssSuIqxL0sbtl53tSqWDNWEorVkvSjThG505LdOGsZkpUEMsp+h+ZS4tqrp4zfcFZssr5SuphbVVQKjW+9tV/yFZbq661rCOOTa+7VH16L51n62fjo/xrsy66dVs3g36350rhqq9Otbm9RfZUlss8qvTfVVWKVpvDt268tt4R7ahcFUKsp+v9gODzWdFsZYzs0BIJsdhkiZzNPxFOSqoM5Z+azEBIhyZvaByJnz5XwCRCSYDiG1zF+wGnmo1ClVYrGdm6c1nQOwG9h0oAKH5iVNZO2tz21me1ty9bVldNYwqBWrGSKxs0yGsW6d7LXrasrprCLK2rmm/d3WuWVJcqVM5OksAWtnbtLJVUFrbTWbxpbMrS7MdLYIunsYBGLnT2LO0snE/J2tsOnfedLYBKPofmIBXVRfywfobiNQ6BjfbxCtt6ZwwOU09hsHKe5ZAzWbpdhqdN++s/PSgimsDCgg5RLrFsMqJaumw2WX3mog4rovNj7sW8Y5Q26wg7nGWus4kovFkuW0iUjKH5lGoUotlKEXV72de6uvXHLTYovqxBdXxaWxUXkXu7dQom6m4ahqsUELabaTLdUMaa8KMUJEspY8lVSU3Jx6d1WzVOHs0967c01wubtRa/GlF3HYRU5NtfLqrFavTcRRrEqSU/Q/P+3RatUvv5fYp+h+f/Eq+p+f9LEAJldHoCf48e9T9T8/6I84K8ytAOmMAzPu0fQ/P+gi7oteIFgEJn84vN68H3KPofn/AEKUwjGZeNc6xL8zOQzKrPapjHPuUD0H594VHb/2XzBEOnE4USV05irtFgLWcbCH3KPp/fep80vX0Vny6bR1/wDsVOHXB9uj6f33tPbsl7oypAehOJnMEs8rcy4+5R9P776/KwTMEsVpyPXC+9t3+Nzk+3p/z/vvrFORDOVZzJC9ZjYyh9MCmbGm0zafY035/wBgrYgKSe3snbWRq2WCp2nBZldIZ2k7SDSrBp0nAkbTKYaTWVOCpyPmNTtZXADWU7cbj8ItKbFQKMTaJtEapWF1fG3h035/1F3MAAFQL7uo+LBgoYreRaBvJ/gtmVOvL4r6+RPDpvz/ALpFll2LA4mfcvs3sy5XzWVkNNsakxgw6YzKbMJ4tQm2zwab8/6lpQE5IciczQXsIlx3hs+LPSxttYX0L9fJoc1sl4nIuLXBgldM2AhfLxasejwab8/74ksIiPnpnpjp8zVtKvOmseRXaW84azNpm2UpmzwZ8F4zX4NN+fsUEk7Zjw3Nutps2Mv2Im0TZGUCNYJpF9HsN9fBpvz/
AL49PXsTw2tsr6JY1ZGqncpG1cZ2fpWuxPY1Fm1PBpvz8enTfZ4tY/saZN1vsuiuLa+Nuum/Px6ZNtfi1DbrfHpUxV0bdFUDx6hN9fXS/n/fDUm+zxWNsTxqNzBlBDg+3cuy3ppvz8WkXy8WsbCePSLmyzZyMlRC2oQbUgdSeVYbFAFymcixrAo5F2i1TNWOum/PpxNnjafHSldtXi1TbrfHpVxVYELjjEPGVzWQeLaRTECsDUpnAs412ilQvAk1IzV00v5FfPbAzzLTaYq+rkE5ROQTlE5ROQTlEPqPgx0xFICkKxwkXao2pnCGba4u1ZvE3ibxN4m8S1t1e0zZKU2V/wD/xAAUEQEAAAAAAAAAAAAAAAAAAACg/9oACAEDAQE/AS/f/8QAHhEAAwACAwEBAQAAAAAAAAAAAAERECAwQFBggHD/2gAIAQIBAT8B8elKUpSl61KUpS/1BiH33qxD2fSvHeZ6XhhCYmsxMLSYmITEJiEIQhOmh+AtVi7vgYvAX6FX4KXjLxl8YswnxP8A/8QAMhAAAgECAwgCAQMEAgMAAAAAAAERAiEQMTISICIwQVFhgXGRoQNAUBMjQlJysWBi0f/aAAgBAQAGPwL+GzZrZqOn0ZI0fk0P7NLP8jN/RqNaNaNdP2a6fs1L7NS+zUvs1L7NS+zXT9mun7NdP2a0a0azP8HX6MqjSzR+S1C+zKkz/BrZrq+zN7ilGRG6uFDHKNKLIywujSh8Jp3nJkeOah+SMjsWL5IvbB/OMu5E3J64Id8LC3GTJSsO4uzLndDnoQSt1DnSUkIuNCgSH84onoPBDwqEWUmn8jGKMiWNFynBFXyWpkfD+d2INJLLqTI2sjIyxyLGnGIJNJkLpBdFrYZGk8EO5ZG07kQJQOpdTI8bu1XkaS2lllON01hZPcyeGX5Hb8jqi8dyYeGTwyeF08MsLF1hk8aW1eN1x0R/U24cnF+pPiCP06Z8F1FRPiWxr/srSyNl5n/bJpK63n0GnRw95LZMqF8EvsT0Kn5sbCNqnqiah/qL/I2UZcOcCVRUl2PA7fZU3e9jY6lLVpzF0RKy3YeeTLVvZ7FK83P7dUeSav1GR4hjcyVxkhnzmRSV0dR/3OH4EtqYKhfBV/xPZXR1TNufQqV0Vyg2eqNpM2ZsJ1dCqOiPDHf2VLzY258wUr7Fc2V03ZTNbw4amjibZZwXqbLNrDj1HCx1dTW8LNl5Zqf2WbRPU1ssXZYvWxSZqP8AkbFFzhbRxVN4a3hCqaNT+93hRp/JEL7MiKkTH2Up5kCWypgaqKmogiomI+TiRScTJWfRkJHRmzSrnQSqpUjiPs6DpSyOjJS/JsRxF0dDYi5kvs2c2dHu/CHaIKvQv04sUz0YoUyKruSbXgbZ/T6FLfQ4alSVTGRSL5LjjqxzXI//AGQ9itfECbzKpWQkkbUdJGmjZ/2P6nWCpf6iarhdih9mTEl4VRVs1KH03VTW4aIpdKKuNCqm3cToqTaZxNfDNSwS21MD2qoNqeHuUuhzcUteyKFS33RS3kXqpZH6d33Niu3ZjfDcq2klPcs18IW1UkyuWkLZc2NitxA2tlFOzbsOomrJ5irbT9lKpjyxbNSdyKonyZ/C/fttNsSVl/4I9or8fwzf8O/4BFlhkRUhQXfO9j587t74WGZGXM9/sKT2LHLGos+b7/YRVkyznfXO9/tZZwVF7kk833+yWN3hcsVLxhkZGXJ94zBCRkZF0WpNJdmZnhkZYX6kY7Suuwtqh0nQlZFs6rISdKZCWORdEdN73gkQWXNp+d3hZxQeBbXTLf8AO97wdRHOhZLGMOFnFilXv/O77xnDPC75LZPfcuZlsYe/O77/AGFNODW98cl7vvkxvsvkxxu8I6u/Je775Pl7zePCXp+jJnDT9nE8EuTHfd98jwt9U8j45UNEbnvkT1e++RPfGKV7PPff8rc976W+3yEu5sdjPltY+991b6p78iew5blQNzpXQzj5MzUO8Dc5Znb5NSE7uextTPwZx8iq9Y+8YjpJljSt/wCORPchqZaKllNjap/x7EfMDhvuZsqpWTMo+DqKnohq9zr/APR4+8dTNWCnDrh1Op1Oo3yEhz1NJCVjTkjLwTslt5rGJP/EACkQAQACAgEEAQUAAgMBAAAAAAEAESExQRBRYXEgMIGRobHB8UDR4fD/2gAIAQEAAT8hyaM/8uvpqwOsbgH/AGw5n7kD5UHI8HyPtByQHPE7X4Qbgg/1mCQ/2suk/wBY+RrW8b/WJ/ok/wBUlH/f0LxvwzyfyifKh4E+0eD9cRw4eMe4eIPtFOJ6hT/BFdwL7b7yljzN/ReFYpjH1Eb43lyEFBxcqxc89MFojcwbfjpWgSukEQGp4k8DHTfUem68zzvzNA2tfS8fM29WTYra5ah/4mCUCbFaeSA7oEGLEcYn76A0p95xLt86viIXwNSYfuCmmonM3DEKu19Lseu0OhgcsXXo8E900xKUeJ+6TvWqqWAGcUvlgiCQGhoYSWSjxE1RIfLFR5g58+JelSb6f3m/qwLfgQl2243OXcWrZeJ5NQxVMxwzwQT99DfQvAxKqSzcsb26z+TptC0XcZYPVuYdfUKxVWz9mfuOIOAIyiOSLXHFRFHOWf3J+rGKO0BJQ73C2YVvo8vM39KjPXmL4H5lpM/yHoHlO8ZSWUxZN+4chYrT36WcfzGb/Caf9pvsHY6OVbXmXlauW7PzM7a/MRqrdjMG58Rig6KCZXK935mI12QSikRX9WIbCcG57zaGvMPsATuOYmq7Ogv75t6DmjiHMVKx9LHBV6GbU9CImER8yrwT9kBBOhftDaVdiArQKym6p/HRFaR3roUb/lFAeDug6dLwjYR3romWkd6lLogzRZ2qKGkb7RFYjydMF2rvUp7MRUFfE2oezpgvF3roC6FlydZZZzLa95s6O548DCvAu5V2i+wRQYLuqC2ss8MtOrgUuONg2Qtwokshl2I/4DPcjL1tkYVdq8MRdA4cn2gNNDdeYjerg5iAW15xHEFmUcZYRlcbbFuCKrL78MAhQ1nmUO1wUZl/KcrTRGV9+TcaDw/clwKvgJ3oUIWeht5ZvQaSAJsUv2g2/Kr4uGjRay0u0AQwtoxHceV95s6Y4IKYmhpN7dW9ZXoreF3LK4DsZZb28CFhJ78Qby0AYqX1O9wUO0dWZ3bLUcGzyVM8+ABcvsqs6xP1zpP7T+Td7wEHgeSVsu52S6V2Mf2QV4KeI5Q9jtDHBpfMcsejHc8CPK9HDNveVS5vd5kqzt/clj+ftJZSKyJCegVHc8vMehbgPci5TTLmrDwxG/aS1Mu5KoVD9cGKVrb5YM0Ad3TL8Bew2s0BTeOIpRp99NdF7RNdzzGimntAdekZfY+UVo0+4haR8M2s+2IrSPcg9JPcp2FlxBjxtPxGKZbeIvanhn7cGXUwVh9y5XhdhgagTotL3mzo1ne7xNSkCrIIERQrlZiCmCIeUMOgZlqW1qHZpNSu67vEqKKqt8So1P8AZhx7ItVF6eJ/Z/IYNQtamMljDF+cbviXA9Yz/wAgUSLF+LlRr3i4oBkqQuqDzcttXpeIdYehg0FMUlsqodY7rDBy8fFyoKj1coTkgNYUXZol4/E9FS9zZ0B0YsZZObyufq/5Soi3PM8yiO6wqzMPVTUPSNyt5NoS4N1icYY/fUqMzWBh5yy7hESS33n9n86UQ7lT1AtqjGau8NYgTUEmFIglW/yhm1yJSIale4ZdUvMCp4J+YebVlShdiz3A1yUhmcT8sfUDdNweQ/wTnItERQ+1uWFA4Z/PS4zzNnQYAKzpiSyZagGwoc+4w51uAGAtDOIMdQR21Fyw3EGvtCskvM4f/wCEqtZkHaNmtcKkmXp4NRyaHL6nA13hWln2Esphsn+elyrDwAcTEVjSXORMUvzHkx1fuJAA4StsMBeYXVbuuY2an7ICnGvcsMm1/mLwiD0kG6YWIdjA1226jDg0YG2LbCWtrM3+/qc9b6kfotJjBUxAhnPf6H9ps+pfzeh/wH+83/UPlXQpQQe4Zbgyuikmpz9MX982+/qHzmU8qzRMRN7hC66HP1Nnub/qc/
FV4h+kHtKCXWHBPOIfbLqolP1K39w0zz/wQ7yIbll+7/JOAfmDajfJC0V3FfsYtgucDX1LnH731DrQ1u5VCzmUBJabqMW1b5hZb4VmBneLh0xhvmbaU/M3rB+o6cO30+Ib6m99pilUmEwXiHRTYlBqGmAIJuOrRFa/qXvDt9Q6i32XaCA1d4jsmKXKUKpkRX5pMfyv5HPEO31D42JHMC4VfxS2tA8QwJPahYD6u7ofqOfhymIQxFqYMBPNLm9wGzmeaylxZhM82edPBEr57IdulEKpbhcGI/8AgxOnJsCaFog1dIPMh2sBGLEA4w95nwTC+rlSkcEqyqd+QusRibd8czNCpq02YCowcyjIIjtPBPGlEGL3zT8iLh2nm1gmmCXSFvVB2SvpZUsVMTCuchxuA2+zKUcPapeowNTiYFdl/OxDTJ8t8O0Fs2alGdG44M76LlnW+ly/kIlkhltx7Zv0feYRLNSymUZ2XmXfyu+2Xx2QuUZBzFVOWaJmHoBlkhxcvpcvq9Z4mJ2LiuQCmZAiu2F7CKwFlFL0ECa3Lv43jsfic4dvkYZzko7l9BbCaqPCf2JUjkxM77xrIXGoDoDlAOsmBXSon2YcH4fZ/wAdsO36FQTygDqvS68DRMd/1oS7ISZ9Q4CJhluCEx+cyDb+NTUHpXN2jv4b4cfMTLPm+XiMx1vC+zBTNflM1xR1HtGLRgW0TwGfQuAg38Rzh2/OlvuPy5mA/b9CqXWfwsELy/GplCI/OHI/DfDt+eeHL9vnYzRj6Gabd9XhkPLRMrtbW/naV3D4DOH5O1fPr5M8Dk3v5qDtVKjaQUd5ZYYag2WfSpLWyPVO/l73g+XM8oL+hluj+4rxb1HPb+xmWxN4Iw41hBYqudeIow9XMtiBq13i5QI1sE7tg28Kn4W9wZAuhhvAAxyzCi7Vqtpn+8674d9MvYcnEH8Lq78XEUjs6d4Kv52Jwa61K6V08sdxClDW9MJKuSt8XxDn6UExxcrLpVPIds/AXdeJk4OMXnFf4geOxb2JK/gKNKmFNHedzUGy9zJEU25xqeK3Lf7T1jMYFwHdCD1LeIjOTW4G4X5lmcTB6LCvTPDGG6dzxx4cp44x6hvcW5T0Bq+l5TBPaAjQVHorg15IWDwVPbA2mtkKfgEd54KepQuy7mbRzR6CHYZ7J7J7p4GAFyT1QWio5K15n//aAAwDAQACAAMAAAAQyAAA02yACA22w79vASQX9NfqSySSy2ySWpfpvlEkAggkWAQAWVGEgAmAnxhUkigkAAQkEiigmCmjQk2kBgJsEQAkCkGAggjQ0G2GkiCGWSSimQWWSCi6oE0S0kHuC2Sm0gkhiw2iSy2WFfWyWyGWQwwj0WmWQqw1bwWzG2SSWSl0CCEQlWQmSiSC24QU2Fw1pF2Weq2RqK95vN9NlEWWWmimmYSywQUySSmFFmzm0QY1+p0S5GeuH+1wy0A2gEkEkggggJAkgjAgAgkAAkgEAkAwEEkElAAAgkgAAgUgiEAAkAkDSAEkkAAEkQ2ygkggkklUEkGkAkg2iyz2EkkEElAkgkEQAkgWQSmkAggglUG2EggEmGSgyQAAAggF0QEggEEmCW2WWAEAEgHQkkgEAgiCSW2QkkggAHwECgggAkSW2CSEkEkkjEEkEggEAGSSSC2EEkAnEESmXAEggWCSEgAGAkDmW/7/AP8A/tqLbK7CCCACOAb/ALfb/ekS0igMkEUABkC2GWyS2GyAyOQkJEAHkEkGSWWWmw2USSUwEADAgEEiaWW2y2gS2SUggHEggEieS0iA20SS2SAAHAEgGWy2AA0ES22WyC0DlgEh73e0gEEC2SiMkgDAEk3bzyQkkEbEgNEgUnEgkfeWU0Egm9+Szog0Hk2gMG2WUgEE9pJ6/wDJJV6du/8At7bfbr5t90wQKD//xAAdEQADAAIDAQEAAAAAAAAAAAAAAREQUCAwQGBB/9oACAEDAQE/EPXeyEIQhCEJ4ZiEIQhCE+1WmWbzvOl8i70MXlXUuKGLkvDCdDzOc8dKUpS86UeaUpcUpSlKUpSlL43ovzKzOTF0IegZOlaZfRz5p/GPTP7J6Z6Z6Z6Z6Zi0r0z0z0z0zxem875f/8QAHhEAAwACAwEBAQAAAAAAAAAAAAEREDEgITBAQVD/2gAIAQIBAT8QKUrKy8piEJzhCYhHwpSsrKXDZRRWSCCCoqKioq4dHXKlRUVFRUQRkrCsTHhEHxmFhkwvN8Zyfk+M8lh+SHhcFweUTKyuDFyYuTxSlKUpS5pSjeFilKUuEy4pSjKUuVlIiGiExCEITExERDRMTEITEJhrEeYphcENXDVEoWiLhO56YbY2JFJm0nZZjRsShRo1hiHhdokG87QlMbGxcg9C1iFwRC9kxsSgmQpLlDxSsVobbNFZWdi0VIbKwjsrOzsrKd4rEdDFK8VileEPCTZWRpoQR+kEoOhqFDUNsNUjwSeCQ8EZWE7yUVwEPGkJ3F7mG5j9wnS95ejbBa5IbmE7icG4J0eEPCZ0U/RsqZT9L0MXsbmG+jY6GE8VFgmJ94JnRayieKacCH9jIbvgh/xUP5YT3Q/jnxIfwr5EP5GJ5o/RD95j8zMtey0P6WvRaH7p+T9FofwLnfZaH8CF8mg8QhCEIQhCY6OiELymOuPQ1yWh7J8D5r1WhiL8GvjWhlxSlE/JcLhv3Wh792LhCE91ofgubE+TYvZaH4LxpSlL8Gg+a5vwXvoP5l76D5L3Xq8rXNc34LlCEIQhB504k8X4LzpR5Whoh2dkIQhCEIQhCEIQhPGEIQhCEEf/xAAoEAEAAgIBAwQDAQEBAQEAAAABABEhMUFRYXEQgZGhscHR8OHxIED/2gAIAQEAAT8QCgNNpqW9JntM/wD50GFGVKlf/OekvtBvUa8aUzB4mbPfv+YxhvA/qbLz/wCITHyx+4fc8h+pwd4DOC/FP7izJe3+wC3yF+orp8l+ppveE/UFrmg1vgS+UPk+NP8Ay0/8lKf4p/4aIFof66z/AE37iG/83eJLfbvE/wDrEuXwn6iPN4/hNd4TgPwH+we35R+4JyjuT9R7gu4/qO/IH9zVeEfuI5DwD9QelX3T7u2zR4PftH57ZitxxoG1u3LBkWPKRIoG6yPUhbgzPuNwFyCh2uMAI21lhHABgdMxFZKiO05/oiMsSw4pg1inrb0B3kaZ/wCNDe0aNOMQUOnLNqo5DEuF6F+JmC9Yg2EBkRU0dxxxOQnfiFRs2V5I94emyBm6xL9FSwc/wRC5pbIT6r+WOgL96JY0EM9nMYg2T/WMrnk6kMG7I6uYODuAcXLKYVxUV/6MxUDL4ROUp3UWVYOtdYDKLFdHpA/B20acx22XUaiEZN5W+YBTQwVa6S8YlQbqmFSrNNpxr+3D9cQLlgVrS6aSNuCp8zd/xTL8IYuvMtEW4eFcVMQKDLz/ACADzZGw6gc01qUeUu0YZSkRjxHsMOPLqzBy8DAjAmdTkhNl3v8AgnxWG4aE6O/LGIVLc27xUF0FA+IsQKBbh7RKg2MrqB2zeMkv4BSlRbG6i+s4Gv2TT5i4fEMSrzRnkAbye0LSLHu6mp99/M4X+UQwZ6Te8fuGtV0vqXAq8ijMREXe0+ph8kau6n0n4jwhrZqu/eNv6IIzDLAwtjQBu5rHDxFVFVj/AFc/xd2GZyt+4R0idEDzKP7oQGr9pWYaWmX4Jk7W+JcSu01eF58Si/ITNGjVYPCWoB1U+8BWycUpUF40ArMMBfVYPiDAS62kepqy1ByPSAK3nVLumcK0w5su3/MrCotmiMAALZUXFUspgbqYuTqKKmDH0VBhkEbI4Dviq5TXJSrb7dIYgPK4bGqxP/ASmQE2DnzDNH1mkl5bf+NSr+BKup2um8pxKKvC4AIWysidIIbfZIaYJumbe7DcNG/8Cfc9BKI3qe9ekRALraPuIw3DY9GNU8bsa+IqiGwUwSAVeAtgImOlz8y7fCJjaI7CteZ2UwFsDaJNirJTdIj0ndvhh8wI0rekBGlUex5gkiC2w1cLKPJh8ztO57TCDKR7BcCtPQVvtE5waUU34nehyhPEGZU8FXzDbg61LMVwLYlTboQuGIl8/
ahH7A5oWJVCVVb8xKR3hbpZbkn2oHEUiD7OjEvB0I6HXObl4omgAznndQiboAHvVlaQZiL0rIHMOyM6uvBAk1MN5IY6WnwJqXlFal9XgWLpQVBnu/5FsWmihIcCjYaAt8yhJGkwIsxRoG6gaIYFNZYUiowI5Y0K9YvARc+xL+o/TbOVLio2TsgjIbqBdNhosZPMNVXOCP5G8RdRo0dJhshrUeajj0TVMUdeM37R7GeLAHV7TElvo1kxGdKJQZf7A61tNkf9zLIhULrq85+pQb0l7ExXFIrFXfZar3gZW3lXsQAlkjkmzzAASN9eCC+lcx0gJu4Tir8MODGUQVnq7liYZDoceefaW5ZBQRGitFoewFQuo4G5Gqh7Zq6FNx8qZNOMsQjCDPgiosyJyuSXflWrF6RhwGXUH4YLUlaeHa0gkpazseMHaf53Vn135fSj/I7QuQ9HuYj12lRVJ/8AYktbg3TgCOvF+kXaVXNvBmFLo1YP5jvFHfwNK/Fe80jmGizdPaY9lr7DEDQxTKyX7JZqC6UKIiqNgejX6v3hQx6Qr3dO0qrTKjwUX7t+0CrBh3ctdxF7myC3pn+COlMXiFhPS0k+d0r7lm1bXLcDrorB8QmhNXuoeJeSmWYXhWmGJabuL+Jc222xYMs7tq4yVF+I3nDuv7jVVqyrspl2tTbE3mS6L8S5BCrusWmKUpVKo67pQivJLzbvrcv06Ved1NIMcLc1e18wOd6Skllk2c4itec1V5lszF5j7K+oTLSeh0OssoW2q4fQfBYfEErGkcJhJbx6q/uLVVVds7qZITkvFCNzUbCLc+iaXRlaiQoeSh7stVDF3SoRgEURzr8SoG4GnxMnFLEbE7MKFIsKF9pjYGLumVfAHGVqUJgpruutQf7Tksqpvr43UDRMhYWI2DtB1O6KH4g+n0G17w/7dUw3KNnPtOZ+cjz1Jf7WmqF3ZnyBnK+yJUJ2OPd0l9H0IfsjvANqcnmA5gLDF+IuJmb4wWCW/QTvHbMNZ/siJ/WA+5xNMxK3JvMJ1CACqvASwlurP8VHrH8w979okrhVZgeYKdC0wd18SvcBaZT5nmMYpz34IvknD4nQs8ZWrWAxWCaEfRQp0xavJF/UTyOOPOHH4+IBmkWoJ11zK6iAAutfZuKsAFnF1cp+5S1a/wCz5dK2yuE8kgecf59oKujaPwblNUhE0zZPt/km7/uGUXSgvVKk1enKr/Ip3XIHjUIzzVAUsS+upSoO1sbBzBSsIHCKfqCwJyHOW/iXAM10VdRqItLkUtCzTOLjs/MXmi3xN/X4hO0mV9/FEqqQHyX8AHzH0aqU81zMGdd7KEmH0AFovvMMlUGntfTpBSoF6g5r+olOYKGmZ9EFc+YVzMwZjV89Y0ryMR9/1GCFG6NZR/c4Dgwzm48IU3+ZV9UaBHwymD2BaB26QlzlajQEFWzfCH0ygtlRqlqVljjELHbt0Rj0S7LI+YF4JUKPk2wOrG8AZQwpFZYaesasKsK8HVmO0l0M8PTMBW8zY+Jn6gOgAUEab1dq/Ed9M2J8QdoVCiKn9x055KhyhT+iqy7ZSQLhhxnhgOxqYvwlodAbsGVfuZTEYdVolxXB2I3cF0Ap1Tiy4X78QAw0D5hVcWrmqZZeOwoGqX9Su46sTof9iMu1tjciviuxKK1WVQz5gZzLp5nvB7wY9eYrKvb6nouEXFauadWUNzmwhcQNQaiNXcILNOCAeKylBzuEc3Ypb7PTHOZmvQjjiErezn+CZeSVKzLzD/65g0ahlglW7lzj0HNxpwEBCaVNl1r01mpvUI9pz6NziGT0Iw3mVO7347Ep7rubJm/QqMHEZzPMqYgyVKx6X3hAgk2HlqOW78S9J8S/DEvjGJfWh8clbhYGOYZGIo4n29e8SV09Ce8Z7Sxxz/BDXZ9Fs5ldPQmyVmcZjiVNoNSh2RDicwYFOA8GISGq3KpCjdF5gWqUdJXmgnqeIQNLxLBmK1v1OYR3jPrzBhOYzVx8V58dieTn1dw8RuG4zMcR4mYQZl+lzAhpuCGNL8wJQDnUKqedp8uiUW6iAnFyMIOI1TMx1HcJUq9TMPRJ1lVhefPYmItI9AnEqVKz6cxiYgQ3HtOPTbLAMi3EDQCYxiCgIrLZTDeLBi1lVqWQzoLZEOhSCm62bSElG3xDv6YqFehGExxH0sFcvwR3qW0AqUErrErE49Gc+pc23KzKuJFVdKLQOYLEinEbZhCqj1C7jcpWNNkhVZaABXeApPEVkieQSJSkKyZtsB8RjYAeNQzE6+hKgRleiehgWsvwTt98S6ixekVTMupcv1aqobZ9LDAJWaIH/Q94drYXUYWJVZEYkod5i9o8lxBgGOJhRAZZWXGDMJsNXMGZdwTpNS4Mx6s6sKoOXPYn2IsNxnE5lcelei5jVzaDLj/Ilm1/5EGcUV0So7RoLHWHqtdBtiu3TkxKbbxCrQvEo3hKCrYTPoNy8wi5fpUDcZ2MvwTDPyziLmH3CY9H0wbnES5tAD1VF1mND5l1EQ6aolCKaweCVmNtRp7Rdah13D+HOO0RVk3GZvBAzFz6X2nMMepXpxB8j8EdrzKiXNE36eIs5ly5xcCAXiA9I2OYFO0uFYZqXOy5YWujL+pQqQxa7hwJzutsMdh0JklcyXHdYhk55liMeajXn4Il/CUFq+I2CVA9eYy+8pmeX4IaoeZxLn3VLgHMIIaF8LBRxDvFhg6xhm4p4xCo7HtiX6z0I4Y+sIn4IJkTbp8sQoI9Y9E8QhNsYDEVk1TiVnxBQ4o3A6Isc9UgcbbZcvnf1GG9ZBaTtub0biyri7GlLqG/uIMMyM3zmBTPNGfQOx+Iu/oiZGzdZIkyckqmob9Fq4wd/wCCfcYnnjHEHgoqokIyrXpUpKHyQBgKmO0v/wC2Cqf+IwPJKSsxjZNXAKSzR1IIodU3GAUMo+0Xo4BxFu966d33Cpc/npeefQwHO9+kSlEpNk3K9Dm59j+CO2SssSyKCKpD9yJ4cMVKVa7IF0kuaHN+lxHeWdpSFoOIuYoEAaLd9W5RJlSAOyjh9HpmtuHvLbcbopGbN3YtNZYUIXGYoYNgdF6wGRkeZXpWfRLlhCsb/wCKl/efgjPk3ArEhcylrFMsMJSQ+hu4SzmyB2N16W2LWDGruAHl4hbUVq1qZrRKXeWKqyoo2rRuPTqZRaHDKAUKTUdZzl36xG0tcECpBlRkr8zjYQOkHO4etNmd3ZlZ9feM1IZ/glyzrHM5xKzDXo2w3FQyLzMttMOsRAdlk5G9wGgEFtq5hlPnn2IeohVxmWANkPKYZhSuC6ZVsYTi3vOHDU/UEcSobYluR7hEvS9evoelSC0tGG4ziNKOr8E+0yo+mm549B9oatWuOJmZXHbA3KxK6yiz8QKGZwHCuhCZnmerrGsAzY2Q8gJ4lvSvaIbQfMWOBldBMd29Zj2lGb3Xof8Ab9OJeX0QmT5heuTpAdS5cOlpvDrNEYQjd1/ggsFtH00Sz0tJuDzAEeXsOCYm2aZfEeIPKfI4lrl2y8RkGkpCxioN1WH3EqHoxgP3Lo6K2fROyWnB7RAsVaDrDH2Q+efv0uXnmMJUQ2Sxu0jk4qIZ3EjPEdbu/BPvQ1F9d49KKL+DOPmVXoE7lSsyvhAssrUdOInp7M7ROkCahA7K/nj/AHaBjmMqLqM1F5Y+0rHpWIipSl1DCeGN2vlI/svPoEXU3/BOi5h6VK1c0Tcq63K44E+NejuEf1KodXAeWtXx/wBv0wRnTmafQ7TFX4QwfuVK/EtYZzqf29oq5bxtf7p6Bj1YGJjwf++fE5lS7uBmOX4Js8zmeIblXE95egt7rwNwAAAA49XUvPM4QUUL088RvLJW17x9iX6pAhI4A94CEORCiYrrqAIATJDV/WH4gAliWJzCP/w+rqUgRvtOZTU6x0vL8E355l8y5cMw+JU4ys3Q39+ly4cS5vhiVO5veD/vrUp9CblqWweWD9wJUlAqmEXyZMfIa8bA699nuyrsyrCErFe5F8usDvS07MWEUAGLLLz/
ALiFYUgILaPaHbNhFDxHasBKAXAjnuTGN1oY6NsrKMUS+DcMjYQrKaDzFQkSwWPCykRnP5D9xuXCvmfgjt63K+ojV5jCvd+plgACQC3k8QS6REeEg1mWcBkeXPoypUCHWUu4Hv2/mBgZR6O6U9JbmBjvM1PwBggK4DXA1XsZ8y7VbDcEVhjL7RaASTiNpWeveGdSet5Bo5zXiFcEAFXqFMXfvGJXD2zSYyVTHJKJ1NxikeNV7QY2FQuQbxzYQyyrQ71i9xZnyUfJc7+WUFyBWnQeK4gLeQ3u5D5ZCAnaw/Ofz6EVEMsLLPsQQvBZYf1FUYFrWSJwKI7VdUfUyUpbVXMsTwLrOJkwTwf2Yb5Oh/YcU4aP7MtaOx/ZaD8A/sG1k7H9hlaOx/Y7MLL3iQDWJT2ilhiZunjvGqyqS6ubqrETApeNwWmmMdCDh7GgWA2N3TBCFunNbHfMWRg7LKqsuqmDoKo6Z3nOF+YmXt4uBwZgSIWTOHOs424gdlAB0FAZ8/MWY+E/s7HwnYy7f9jbr4n9iRf0QNEVMwuq8+7/ACBvzVf+S4BV6KCwxP/Z" /><p></p>
			  George Thorogood was, is and always will be a bar band rocker at heart.<p></p>On his first major label record 1982's "Bad to the Bone" made for Capitol, he and his tight-as-duck-feathers band don't change a single note of their hard rocking, beer guzzling sound. Only the size of the bar has changed; Thorogood's slide stings like a big and nasty bee, his voice is as dude-next-door as always, the rhythm section favors brute force over subtlety at all times and Hank Carter blows a marvelously yakky post-Clemons sax. <p></p>They cover classic slabs of American rock and blues like the Isley Brothers' "Nobody But Me," Chuck Berry's "No Particular Place to Go," John Lee Hooker's "New Boogie Chillun," soulful ballads like Jimmy Reed's "It's a Sin" and Albert King's "As the Years Go Passing By," and most surprisingly, a restrained and almost thoughtful version of Bob Dylan's ballad "Wanted Man." The originals are good, too, with suitably raw rocker "Back to Wentzille" leading things off and the huge, timeless hit "Bad to the Bone" providing the albums' highlight. Next to Move It on Over, this is Thorogood's finest work and established him as one of the unsung heroes of the age of AOR.
			</p>
			<Comms url="/react/api/allcomm" />
			</div>
		);
	}
});

module.exports = Home;
